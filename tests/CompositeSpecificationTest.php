<?php

declare(strict_types=1);

namespace Boulzy\Tests\Specification;

use Boulzy\Specification\Specification;
use Boulzy\Tests\Specification\Implementation\User;
use PHPUnit\Framework\Attributes\DataProviderExternal;
use PHPUnit\Framework\TestCase;

final class CompositeSpecificationTest extends TestCase
{
    #[DataProviderExternal(className: AndSpecificationTest::class, methodName: 'provider')]
    public function testAnd(Specification $a, Specification $b, User $candidate, bool $expected): void
    {
        $this->assertSame($expected, $a->and($b)->isSatisfiedBy($candidate));
        $this->assertSame($expected, $b->and($a)->isSatisfiedBy($candidate));
    }

    #[DataProviderExternal(className: AndNotSpecificationTest::class, methodName: 'provider')]
    public function testAndNot(Specification $a, Specification $b, User $candidate, bool $expected): void
    {
        $this->assertSame($expected, $a->andNot($b)->isSatisfiedBy($candidate));
    }

    #[DataProviderExternal(className: OrSpecificationTest::class, methodName: 'provider')]
    public function testOr(Specification $a, Specification $b, User $candidate, bool $expected): void
    {
        $this->assertSame($expected, $a->or($b)->isSatisfiedBy($candidate));
        $this->assertSame($expected, $b->or($a)->isSatisfiedBy($candidate));
    }

    #[DataProviderExternal(className: OrNotSpecificationTest::class, methodName: 'provider')]
    public function testOrNot(Specification $a, Specification $b, User $candidate, bool $expected): void
    {
        $this->assertSame($expected, $a->orNot($b)->isSatisfiedBy($candidate));
    }

    #[DataProviderExternal(className: NotSpecificationTest::class, methodName: 'provider')]
    public function testNot(Specification $specification, User $candidate, bool $expected): void
    {
        $this->assertSame($expected, $specification->not()->isSatisfiedBy($candidate));
    }
}
