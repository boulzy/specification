<?php

declare(strict_types=1);

namespace Boulzy\Tests\Specification;

use Boulzy\Specification\NotSpecification;
use Boulzy\Specification\Specification;
use Boulzy\Tests\Specification\Implementation\User;
use Boulzy\Tests\Specification\Implementation\UserIsEnabledSpecification;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

final class NotSpecificationTest extends TestCase
{
    /**
     * @return iterable<mixed[]>
     */
    public static function provider(): iterable
    {
        $spec = new UserIsEnabledSpecification();

        $userA = new User(true);
        $userB = new User(false);

        yield [$spec, $userA, false];
        yield [$spec, $userB, true];
    }

    #[DataProvider('provider')]
    public function testIsSatisfiedBy(Specification $specification, User $candidate, bool $expected): void
    {
        $this->assertSame($expected, (new NotSpecification($specification))->isSatisfiedBy($candidate));
    }
}
