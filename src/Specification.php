<?php

declare(strict_types=1);

namespace Boulzy\Specification;

interface Specification
{
    /**
     * Checks if the specification is satisfied by the candidate.
     *
     * @param mixed $candidate The candidate that can satisfy the specification
     *
     * @return bool `true` if the candidate satisfies the specification, `false` otherwise
     */
    public function isSatisfiedBy(mixed $candidate): bool;

    /**
     * Creates a new specification that requires that this specification and the other one are satisfied.
     *
     * @param Specification $specification The second specification to satisfy
     *
     * @return Specification The new specification
     */
    public function and(Specification $specification): Specification;

    /**
     * Creates a new specification that requires that this specification is satisfied and not the other one.
     *
     * @param Specification $specification The specification to not satisfy
     *
     * @return Specification The new specification
     */
    public function andNot(Specification $specification): Specification;

    /**
     * Creates a new specification that requires that this specification or the other one is satisfied.
     *
     * @param Specification $specification The specification to satisfy
     *
     * @return Specification The new specification
     */
    public function or(Specification $specification): Specification;

    /**
     * Creates a new specification that requires that this specification is satisfied or not the other one.
     *
     * @param Specification $specification The specification to satisfy
     *
     * @return Specification The new specification
     */
    public function orNot(Specification $specification): Specification;

    /**
     * Creates a new specification that requires that this specification is not satisfied.
     *
     * @return Specification The new specification
     */
    public function not(): Specification;
}
