<?php

declare(strict_types=1);

namespace Boulzy\Specification;

/**
 * Specification satisfied when the wrapped one is not.
 */
final class NotSpecification extends CompositeSpecification
{
    /**
     * @param Specification $wrappedSpecification The specification that must no be satisfied
     */
    public function __construct(
        private Specification $wrappedSpecification
    ) {
        $this->wrappedSpecification = $wrappedSpecification;
    }

    public function isSatisfiedBy($candidate): bool
    {
        return !$this->wrappedSpecification->isSatisfiedBy($candidate);
    }
}
